var db = require('../fn/db1');


exports.loadAll = () => {
    var sql = 'select * from room_type';
    return db.load(sql);
}
exports.loadPhieuDat = () => {
    var sql =`SELECT R1.reservation_id, R3.type_name, R1.renter_id, R1.number_of_days, R2.name, R2.address, R2.phone,R2.gender, R1.note
                     FROM reservation R1 LEFT JOIN renter R2 ON (R1.renter_id = R2.renter_id)
					LEFT JOIN room_type R3 ON (R1.room_type_id = R3.room_type_id)`;
    // return db.load(sql);
    return db.load(sql);
}
exports.loadPhong = () => {
    var sql =`SELECT * from room`;
    return db.load(sql);
}

exports.loadPhongCapNhat = (c) => {
    var sql =`select * from room_type where room_type_id = ${c} `;
    return db.load(sql);
}
exports.loadFood = () => {
    var sql =`SELECT * from food`;
    return db.load(sql);
}

exports.add = (c) => {
    var sql = `INSERT INTO tenancy_card(renter_id,room_id,total_price,start_day) values('${c.renter_id}','${c.room_id}','${c.total_price}','${c.start_day}')`;
    return db.save(sql);
}
exports.updateStatus = (c) => {
    var sql = `UPDATE room SET room_status = 1 WHERE room_id = ${c.room_id}`;
    return db.save(sql);
}
exports.updateRenter = (c) => {
    var sql = `UPDATE room SET id_renter = ${c.renter_id} WHERE room_id = ${c.room_id}`;
    return db.save(sql);
}


// exports.single = id => {
// 	var sql = `select * from categories where CatID = ${id}`;
// 	return db.load(sql);
// }

// exports.add = admin => {
// 	var sql = `insert into categories(CatName) values('${admin.CatName}')`;
// 	return db.save(sql);
// }

// exports.delete = id => {
// 	var sql = `delete from categories where CatID = ${id}`;
// 	return db.save(sql);
// }

// exports.update = admin => {
// 	var sql = `update categories set CatName = '${admin.CatName}' where CatID = ${admin.CatID}`;
// 	return db.save(sql);
// }