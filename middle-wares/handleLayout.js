module.exports = (req, res, next) => {

    if (req.session.staff != undefined) {
		res.locals.layoutVM = {
            staffName: req.session.staff.staff_name
        };
    }
    next();
};