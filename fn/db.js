var mysql = require('mysql');

// exports.load = sql => {
// 	return new Promise((resolve, reject) => {
// 		var cn = mysql.createConnection({
// 			host: 'localhost',
// 			port: 3306,
// 			user: 'root',
// 			password: '',
// 			database: 'hotel'
// 		});


// 		cn.connect();

// 		cn.query(sql, function(error, rows, fields){
// 			if (error) {
// 				reject(error);
// 				// statement
// 			} else {
// 				resolve(rows);
// 			}

// 			cn.end();
// 		});
// 	});
// }

// exports.save = sql => {
//     return new Promise((resolve, reject) => {
//         var cn = mysql.createConnection({
//             host: 'localhost',
//             port: 3306,
//             user: 'root',
//             password: '',
//             database: 'hotel'
//         });

//         cn.connect();

//         cn.query(sql, function(error, value) {
//             if (error) {
//                 reject(error);
//             } else {
//                 resolve(value);
//             }

//             cn.end();
//         });
//     });
// }

module.exports = {
    connectDatabase: () => {
        var mysql = require('mysql');
        var cn = mysql.createConnection({
            host: 'localhost',
            port: 3306,
            user: 'root',
            password: '',
            database: 'hotel'
        });
        cn.connect();
        return cn;
    }
}