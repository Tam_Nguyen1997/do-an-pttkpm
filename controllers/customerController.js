var express = require('express');
var db = require('../fn/db');
var router = express.Router();



router.get('/rooms', (req, res) => {
    db.connectDatabase().query("select * from room_type", function (err, rows) {
        if (err) throw err;
        var vm = {
            rooms: rows
        };
        res.render('customer/rooms', vm)
    })
});

router.get('/service', (req, res) => {
    db.connectDatabase().query("select * from service", function (err, rows) {
        if (err) throw err;
        var vm = {
            services: rows
        };
        res.render('customer/service', vm)
    })
});

router.get('/food', (req, res) => {
    db.connectDatabase().query("select * from food", function (err, rows) {
        if (err) throw err;
        var vm = {
            foods: rows
        };
        res.render('customer/food', vm)
    })
});

router.get('/room-details/:id', (req, res) => {
    var pid = req.params.id;
    db.connectDatabase().query("select * from room_type where room_type_id = ?", [pid], function (err, row) {
        if (err) throw err;
        var vm = {
            room: row[0]
        };
        res.render('customer/room-details', vm)
    })
});

router.get('/introduction', (req, res) => {
    res.render('customer/introduction')
});

router.post('/add', (req, res) => {
    const renterdata = {
        name: req.body.name,
        address: req.body.address,
        phone: req.body.phone,
        gender: req.body.gender
    }
    db.connectDatabase().query("INSERT INTO renter SET ?", renterdata, function (err, rows) {
        if (err) throw err;
        db.connectDatabase().query("select renter_id from renter where name = ? AND phone = ?", [req.body.name,req.body.phone], function (err, row) {
            if (err) throw err;
            const reservationdata = {
                room_type_id: req.body.room_type_id,
                renter_id: row[0].renter_id,
                number_of_days: req.body.number_of_days,
                note: req.body.note
            }
            db.connectDatabase().query("INSERT INTO reservation SET ?", reservationdata, function (err, rows) {
                if (err) throw err;
                res.render('./')
            })
        })
    })
});

module.exports = router;

