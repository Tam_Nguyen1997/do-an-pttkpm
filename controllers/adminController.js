var express = require('express')
var db = require('../fn/db');
// var db = require('../fn/db');

var SHA256 = require('crypto-js/sha256');
var restrict = require('../middle-wares/restrictStaff');
var adminRepo = require('../repos/adminRepo');
var accountRepo = require('../repos/accountRepo');
var router = express.Router();

router.get('/login', (req, res) => {
    res.render('admin/login')
});
router.get('/logout', (req, res) => {
    req.session.staff = null;
    res.render('./')
});

router.post('/login', (req, res) => {
    var user = {
        email: req.body.email,
        password: SHA256(req.body.password).toString()
    };
    db.connectDatabase().query("select * from staff where email = ? AND password = ?", [user.email, user.password], function (err, rows) {
        if (rows.length > 0) {
            req.session.staff = rows[0];
            res.redirect('/admin/');
            return;
        } else {
            var vm = {
                showError: true,
                errorMsg: 'Login failed'
            };
            res.render('admin/login', vm);
        }
    });
});


router.get('/', restrict, (req, res) => {
    adminRepo.loadAll().then(rows => {
        var vm = {
            rooms: rows
        };
        res.render('admin/home', vm);
    });
});

router.get('/updatePrice/:id', restrict, (req, res) => {
    var pid = req.params.id;
    db.connectDatabase().query("select * from room_type where room_type_id = ?", [pid], function (err, row) {
        if (err) throw err;
        var vm = {
            room: row[0]
        };
        res.render('admin/updatePrice', vm)
    })
});
// router.get('/updatePrice/:id', restrict, (req, res) => {
//     var pid = req.params.id;
//     db.loadPhongCapNhat(req.params.id).then(value => {
//         var vm = {
//             room: value
//         };
//         res.render('./booked');
//     })
// });

router.post('/updatePrice/:id', (req, res) => {
    // update categories set CatName = '${category.CatName}' where CatID = ${category.CatID}
    var pid = req.params.id;
    db.connectDatabase().query(`update room_type set price = '${req.body.price}' where room_type_id = ?`, [pid], function (err, row) {
        if (err) throw err;
        
        res.redirect('/admin');
    })
    
});


router.get('/booked',restrict, (req, res) => {
    var ph_dat = adminRepo.loadPhieuDat();
    var allphong = adminRepo.loadPhong();
    var thuc_an = adminRepo.loadFood();
    
    Promise.all([ph_dat, allphong,thuc_an]).then(([ph_dat, allphong,thuc_an]) => {
        var vm = {
            // categories: LS,
            // nhaXuatBan: NXB
            showAlert: false,
            phieu_Dat: ph_dat,
            phong: allphong,
            food: thuc_an,
        };
        res.render('admin/booked', vm);
    });
});
router.post('/booked', (req, res) => {
    adminRepo.add(req.body).then(value => {
        res.redirect('./booked');
    }).catch(err => {
        res.end('Fail CMNR');
    });
    adminRepo.updateStatus(req.body); //Cập nhật trạng thái
    adminRepo.updateRenter(req.body); //Cập nhật người thuê
});

module.exports = router;